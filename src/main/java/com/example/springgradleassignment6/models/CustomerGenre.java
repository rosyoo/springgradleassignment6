package com.example.springgradleassignment6.models;

public class CustomerGenre {
    private String firstName;
    private String lastName;
    private String genreName;
    private String mostPlayedGenre;

    public CustomerGenre(String firstName, String lastName, String genreName, String mostPlayedGenre) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.genreName = genreName;
        this.mostPlayedGenre = mostPlayedGenre;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName){
        this.genreName = genreName;
    }

    public String getMostPlayedGenre() {
        return mostPlayedGenre;
    }

    public void setMostPlayedGenre(String mostPlayedGenre) {
        this.mostPlayedGenre = mostPlayedGenre;
    }

}
