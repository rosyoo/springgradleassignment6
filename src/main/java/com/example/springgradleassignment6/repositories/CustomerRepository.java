package com.example.springgradleassignment6.repositories;


import com.example.springgradleassignment6.models.*;
import com.example.springgradleassignment6.utils.ConnectionHelper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.example.springgradleassignment6.utils.ConnectionHelper.CONNECTION_URL;

public class CustomerRepository {

    private String URL = ConnectionHelper.CONNECTION_URL;

    //GET ALL CUSTOMERS FORM DB
    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> returnCustomers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(URL)){
            //Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer");
            //Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                returnCustomers.add(new Customer(
                        resultSet.getString("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                ));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return returnCustomers;
    }

    //GET CUSTOMER BY ID
    public Customer getCustomerById(String id){
        Customer returnCustomer = null;
        try(Connection conn = DriverManager.getConnection(URL)){
            //Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = ?");
            preparedStatement.setString(1, id);
            //Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                returnCustomer = new Customer(
                        resultSet.getString("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return returnCustomer;
    }

    //GET CUSTOMER BY NAME
    public Customer getCustomerByName(String name){
        Customer returnCustomer = null;
        try(Connection conn = DriverManager.getConnection(URL)){
            //Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE ? OR LastName LIKE ?");
            preparedStatement.setString(1, name);
            //Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                returnCustomer = new Customer(
                        resultSet.getString("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return returnCustomer;
    }

    //GET SUBSET OF CUSTOMER DATA
    public ArrayList<Customer> getSubsetOfCustomers(String limit, String offset){
        ArrayList<Customer> returnCustomers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(URL)){
            //Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer LIMIT ? OFFSET ?");
            preparedStatement.setString(1,limit);
            preparedStatement.setString(2,offset);

            //Execute query
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                returnCustomers.add( new Customer(
                        resultSet.getString("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email"))
                );
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return returnCustomers;
    }

    //ADD CUSTOMER
    public Boolean addCustomer(Customer customer){
        Boolean success = false;
        try(Connection conn = DriverManager.getConnection(URL)){
            //Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO Customer(CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email) VALUES(?,?,?,?,?,?,?)");
            preparedStatement.setString(1, customer.getCustomerId());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhone());
            preparedStatement.setString(7, customer.getEmail());

            //Execute query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);

        }catch(SQLException e){
            e.printStackTrace();
        }
        return success;
    }

    //UPDATE EXISTING CUSTOMER
    public Boolean updateCustomer(String id, Customer customer){
        Boolean success = false;
        try(Connection conn = DriverManager.getConnection(URL)){
            //Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("UPDATE Customer SET FirstName=?, LastName=?, Country=?, PostalCode=?, Phone=?, Email=? WHERE CustomerId=?");
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhone());
            preparedStatement.setString(6, customer.getEmail());
            preparedStatement.setString(7, id);

            //Execute query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);

        }catch(SQLException e){
            e.printStackTrace();
        }
        return success;
    }

    //GET NUMBER OF CUSTOMERS IN EACH COUNTRY IN DESCENDING ORDER
    public ArrayList<CustomerCountry> getCustomerCountInCountry(){
        ArrayList<CustomerCountry> returnCustomers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(URL)){
            //Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Country, COUNT(Country) AS numOfCountries FROM Customer GROUP BY Country ORDER BY numOfCountries DESC");

            //Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                returnCustomers.add(new CustomerCountry(
                        resultSet.getString("Country"),
                        resultSet.getString("numOfCountries")
                ));
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return returnCustomers;
    }

    //GET HIGHEST SPENDING CUSTOMERS IN DESCENDING ORDER
    public ArrayList<CustomerSpender> getHighestSpendingCustomers(){
        ArrayList<CustomerSpender> returnCustomers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(URL)){
            //Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Customer.CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email, Invoice.Total FROM Customer INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId ORDER BY Invoice.Total DESC");

            //Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                returnCustomers.add(new CustomerSpender(
                        resultSet.getString("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email"),
                        resultSet.getString("total")
                ));
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return returnCustomers;
    }

    //GET MOST PLAYED GENRE OF CUSTOMER
    public ArrayList<CustomerGenre> getMostPlayedGenreOfCustomer(String id){
        ArrayList<CustomerGenre> returnCustomers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(URL)){
            //Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("WITH mostPlayedGenres AS (SELECT Customer.FirstName, Customer.LastName, Genre.Name, COUNT(Track.GenreId) AS mostPlayedGenre FROM Customer JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId JOIN Track ON InvoiceLine.TrackId = Track.TrackId JOIN Genre ON Track.GenreId = Genre.GenreId WHERE Customer.CustomerId = ? GROUP BY Genre.GenreId) SELECT FirstName, LastName, Name, mostPlayedGenre FROM mostPlayedGenres WHERE (SELECT MAX(mostPlayedGenre) FROM mostPlayedGenres) = mostPlayedGenre");
            preparedStatement.setString(1,id);

            //Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                returnCustomers.add(new CustomerGenre(
                        resultSet.getString("firstName"),
                        resultSet.getString("lastName"),
                        resultSet.getString("name"),
                        resultSet.getString("mostPlayedGenre")
                ));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return returnCustomers;
    }

    //GET FIVE RANDOM ARTISTS
    public ArrayList<Artist> getRandomArtists(){
        ArrayList<Artist> randomArtists = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(URL)){
            //Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Name FROM Artist ORDER BY RANDOM() LIMIT 5");

            //Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                randomArtists.add(new Artist(
                        resultSet.getString("Name")
                ));
            }

        }catch(SQLException e){
            e.printStackTrace();
        }
        return randomArtists;
    }
}
