package com.example.springgradleassignment6.controllers;

import com.example.springgradleassignment6.repositories.CustomerRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewController {
    CustomerRepository customerRepository = new CustomerRepository();

    @GetMapping("/")
    public String index(Model artist){
        artist.addAttribute("artists", customerRepository.getRandomArtists());
        return "index";
    }

}
