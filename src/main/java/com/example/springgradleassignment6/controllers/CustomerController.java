package com.example.springgradleassignment6.controllers;

import com.example.springgradleassignment6.models.Customer;
import com.example.springgradleassignment6.models.CustomerCountry;
import com.example.springgradleassignment6.models.CustomerGenre;
import com.example.springgradleassignment6.models.CustomerSpender;
import com.example.springgradleassignment6.repositories.CustomerRepository;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CustomerController {

    //private CustomerDAO customerDAO = new CustomerDAO();
    private CustomerRepository customerRepository = new CustomerRepository();

    //Return all customers
    //example: http://localhost:8080/api/customers
    @GetMapping("/api/customers")
    public List<Customer> getAllCustomers(){
        return customerRepository.getAllCustomers();
    }

    //Return a customer by ID
    //example: http://localhost:8080/api/customers/1
    @GetMapping("/api/customers/{id}")
    public Customer getCustomerById(@PathVariable String id){
        return customerRepository.getCustomerById(id);
    }

    //Return a customer by name
    //example: http://localhost:8080/api/customers/search/Enrique
    @GetMapping("/api/customers/search/{name}")
    public Customer getCustomerByName(@PathVariable String name){
        return customerRepository.getCustomerByName(name);
    }

    //Return susbset of customers
    //example: http://localhost:8080/api/customers/page/10/20
    @GetMapping("/api/customers/page/{limit}/{offset}")
    public List<Customer> getSubsetOfCustomers(@PathVariable String limit, @PathVariable String offset){
        return customerRepository.getSubsetOfCustomers(limit, offset);
    }

    //Add a customer
    @PostMapping("/api/customers/add")
    public Boolean addCustomer(@RequestBody Customer customer){
        return customerRepository.addCustomer(customer);
    }

    //Update customer
    @PutMapping("/api/customers/{id}")
    public Boolean updateCustomer(@PathVariable String id, @RequestBody Customer customer){
        return customerRepository.updateCustomer(id, customer);
    }

    //Return customers by countries in descending order
    //example: http://localhost:8080/api/customers/countries
    @GetMapping("/api/customers/countries")
    public ArrayList<CustomerCountry> getCustomerCountInCountry(){
        return customerRepository.getCustomerCountInCountry();
    }

    //Return customers by highest spending in descending order
    //example: http://localhost:8080/api/customers/spenders
    @GetMapping("/api/customers/spenders")
    public ArrayList<CustomerSpender> getHighestSpendingCustomers(){
        return customerRepository.getHighestSpendingCustomers();
    }

    //Return customer's most played genre
    //example: http://localhost:8080/api/customers/40/popular/genre
    @GetMapping("/api/customers/{id}/popular/genre")
    public ArrayList<CustomerGenre> getMostPlayedGenreOfCustomer(@PathVariable String id){
        return customerRepository.getMostPlayedGenreOfCustomer(id);
    }

}
