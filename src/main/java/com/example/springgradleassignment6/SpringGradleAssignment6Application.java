package com.example.springgradleassignment6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGradleAssignment6Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringGradleAssignment6Application.class, args);
    }

}
